# Contributing

When contributing to this repository, please first discuss the change you 
wish to make via issue, email, or any other method with the owners of this 
repository before making a change.

Please note we have a [code of conduct](CODE_OF_CONDUCT.md), please follow it 
in all your interactions with the project.

## Request for contributions

Please contribute to this repository if any of the following is true:
- You have expertise in community development, communication, or education
- You want open source communities to be more collaborative and inclusive
- You want to help lower the burden to first time contributors

## Contributions

Prerequisites:

- Familiarity with 
    [pull requests](https://help.github.com/articles/using-pull-requests) and 
    [issues](https://guides.github.com/features/issues/).
- Knowledge of [Markdown](https://help.github.com/articles/markdown-basics/) 
    for editing `.md` documents.

In particular, this community seeks the following types of contributions:

- **Ideas**: participate in an issue thread or start your own to have your 
    voice heard.
- **Resources**: submit a pull request to add to 
    [RESOURCES.md](docs/RESOURCES.md) with links to related content.
- **Outline sections**: help us ensure that this repository is comprehensive. 
    If there is a topic that is overlooked, please add it, even if it is just 
    a stub in the form of a header and single sentence. 
    Initially, most things fall into this category.
- **Writing**: contribute your expertise in an area by helping us expand the 
    included content.
- **Copy editing**: fix typos, clarify language, and generally improve the 
    quality of the content.
- **Formatting**: help keep content easy to read with consistent formatting.


## Development environment setup

> **[?]**
> Proceed to describe how to setup local development environment.
> e.g:

To set up a development environment, please follow these steps:

1. Clone the repo

```sh
git clone https://github.com/{{cookiecutter.github_username}}/{{cookiecutter.repo_slug}}
```

2. TODO

## Issues and feature requests

Have you found a bug in the source code, a mistake in the documentation, or 
maybe you'd like a new feature? 
You can help us by 
[submitting an issue](https://gitdab.com/${REPO_OWNER}/${REPO_NAME}/issues). 
Before you create an issue, make sure to search the issue archive -- your 
issue may have already been addressed!

Please try to create bug reports that are:

- _Reproducible._ Include steps to reproduce the problem.
- _Specific._ Include as much detail as possible: which version, what environment, etc.
- _Unique._ Do not duplicate existing opened issues.
- _Scoped to a Single Bug._ One bug per report.

**Even better: Submit a pull request with a fix or new feature!**

### How to submit a Pull Request

1. Search our repository for open or closed
   [Pull Requests](https://gitdab.com/${REPO_OWNER}/${REPO_NAME}/pulls)
   that relate to your submission. 
   You don't want to duplicate effort.
2. Fork the project.
3. Create your feature branch (`git checkout -b feat/amazing_feature`).
4. Commit your changes (`git commit -m 'feat: add amazing_feature'`) uses 
    [conventional commits](https://www.conventionalcommits.org), so please 
    follow the specification in your commit messages.
5. Push to the branch (`git push origin feat/amazing_feature`)
6. [Open a Pull Request](https://gitdab.com/${REPO_OWNER}/${REPO_NAME}/compare)
